import time

struct Timer {
mut:
	interval int
	// tick fn()
	elapsed   int
	prev_time i64
}

fn Timer.new() Timer {
	return Timer{
		prev_time: time.now().unix_milli()
	}
}

fn (mut t Timer) update() bool {
	current_time := time.now().unix_milli()
	t.elapsed += int(current_time - t.prev_time)
	t.prev_time = current_time
	if t.elapsed >= t.interval {
		t.elapsed -= t.interval
		// t.tick()
		return true
	}
	return false
}
